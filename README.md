# Marksman
## The Markup Source Manager

This program is designed to insert metadata (such as title, date, and other front matter) into text files in preparation for their inclusion as content for static site generators such as Pelican and Hugo. Your chosen static site generators may not have an easy way of taking large numbers of existing documents and converting them for such use. This project aims to fill that gap in capability.

## Features
- Apply category, dates, authors, tags and more to multiple files in seconds
- Includes the predefined front matter variables for Pelican and Hugo
- Import and use your own dictionary of front matter variables
- Choose YAML, TOML, or RST text formatting for front matter
- Three kinds of sources for the date option: filename, creation time, and modification time

## Status
Marksman v0.1 is currently in beta
