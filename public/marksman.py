#!/usr/bin/env python3
"""
===============================================================================
Marksman inserts and formats metadata (front matter) into content files to prep
them for use with static site generators such as Pelican and Hugo.
LICENSE: MIT, NO WARRANTIES are offered or implied for ANY use of this software
AUTHOR: Ike Davis
===============================================================================
"""


import sys
import json
from pathlib import Path as p
from os import stat
from collections import deque as dq
from collections import OrderedDict
from datetime import date
from common import read_list, write_list
from arguments import build_args

__author__ = "Ike Davis"
__copyright__ = "Copyright 2020, Ike Davis"
__license__ = "MIT"
__version__ = "v0.1 beta"


class Marksman:
    """format and insert front matter for static site generator content"""

    def __init__(self, **kwargs):
        self._custom_args = OrderedDict(kwargs)  # custom front matter variables
        self._state = dict(
            date_type='created'  # source of date: create, filename, mod
        )
        # front matter keys are pre-defined Pelican and Hugo variables
        # the formatter will ignore any key with a value that returns False
        # add arguments to supply values as needed to build a project
        self.meta_dict = OrderedDict({
            'title': None,  # extracted from filename
            'date': None,  # extracted from creation timestamp
            'modified': None,
            'tags': None,
            'keywords': None,
            'category': None,
            'slug': None,
            'author': None,
            'authors': None,
            'summary': None,
            'lang': None,
            'translation': None,
            'status': None,
            'template': None,
            'save_as': None,
            'aliases': None,
            # Hugo front matter variables
            'audio': None,
            'cascade': None,
            'description': None,
            'draft': None,
            'expiryDate': None,
            'headless': None,
            'images': None,
            'isCJKLanguage': None,
            'layout': None,
            'lastmod': None,
            'linkTitle': None,
            'markup': None,
            'outputs': None,
            'publishDate': None,
            'resources': None,
            'series': None,
            'type': None,
            'url': None,
            'videos': None,
            'weight': None,
            '<taxonomies>': None})

    def get_source_list(self, path, ext='md'):
        """collect file paths recursively"""
        source = p(path)
        return source.glob(f'**/*.{ext}')

    def format_header(self, dictionary, markup='default'):
        """build header body with chosen format"""
        header = []
        sep1 = ''
        if markup == 'toml':
            sep2 = ' = '
        elif markup == 'rst':
            sep1 = ':'
            sep2 = ': '
        else:
            sep2 = ': '

        for key, value in dictionary.items():
            if value:
                if markup == 'rst' and key == 'title':
                    header.append('{}\n{}\n'.format(value,
                                                    '#' * len(value)))
                else:
                    header.append(f'{sep1}{key}{sep2}{value}')
        # lifted from header formatter
        if markup == 'yaml':
            header.insert(0, '---')
            header.append('---')
        elif markup == 'toml':
            header.insert(0, '+++')
            header.append('+++')
        header.append('<!-- front matter produced with marksman -->')
        formatted_header = '\n'.join(header)

        return formatted_header

    def insert_auto_vals(self, post, dictionary, markup='default'):
        """values requiring code to resolve are handled here"""
        post_file = p(post)
        # pre-formatted fields
        dictionary['title'] = post_file.stem.replace('-', ' ').title()
        if self._state['date_type'] == 'created':
            dictionary['date'] = str(
                date.fromtimestamp(p.stat(post_file).st_ctime))
        elif self._state['date_type'] == 'filename':
            dictionary['date'] = post_file.stem
        elif self._state['date_type'] == 'mod':
            dictionary['date'] = str(
                date.fromtimestamp(p.stat(post_file).st_mtime))

        # build header
        header_auto = self.format_header(dictionary, markup)
        return header_auto

    def format_list_value(self, list_val, markup):
        """converts list to format used by given markup"""
        if markup == 'yaml':
            formatted_list_val =  '\n- ' + '\n- '.join(list_val)
        elif markup == 'toml':
            formatted_list_val =  str(list_val)
        elif markup == 'rst':
            formatted_list_val =  '; '.join(list_val)
        else:
            formatted_list_val =  ', '.join(list_val)
        return formatted_list_val

    def populate_dict(self, dictionary, args, markup='default'):
        """insert values from commandline that match keys of given dictionary"""
        args_dict = vars(args)
        for k, v in args_dict.items():
            # select only strings and lists for front matter values
            if k in dictionary:
                if isinstance(args_dict[k], str):
                    dictionary[k] = v
                elif isinstance(args_dict[k], list):
                    dictionary[k] = self.format_list_value(
                        v, markup)
                elif isinstance(dictionary[k], list):
                    dictionary[k] = self.format_list_value(
                        dictionary[k], markup)
                else:
                    continue
            else:
                continue
        return dictionary

    def write_front_matter(self, source_list, dest, dictionary, args,
                           verbose=False, markup='default'):
        """Insert front matter into each file and output to destination path"""
        self.populate_dict(dictionary, args, markup)
        post_count = 0
        for post in source_list:
            try:
                post_list = dq(read_list(str(post)))
            except IsADirectoryError:
                continue
            except FileNotFoundError:
                continue
            md_header = self.insert_auto_vals(post, dictionary, markup)
            post_list.appendleft(md_header)
            write_list(str(p(dest, p(post).name)), post_list)
            if verbose:
                print(str(post))
                post_count += 1
        if verbose:
            print('file(s) processed:', post_count)
        return [post_list, post_count]


def main(*args):
    """commandline processesing"""
    args = build_args()
    mm = Marksman()

    # dictionary that contains front matter keys/values
    # add arguments named same as keys to arguments.py if cli access desired
    if not args.test:
        if args.json and args.json.endswith('.json'):
            try:
                with open(args.json, 'r') as data:
                    applied_dict = json.loads(data.read(), encoding='utf-8')
            except FileNotFoundError:
                sys.exit('file does not exist')
        else:
            applied_dict = mm.meta_dict

        # handle all other args and write files
        if args.date:
            mm._state['date_type'] = args.date

        source_pages_path = mm.get_source_list(args.paths[0])
        output_pages_path = args.paths[1]
        mm.write_front_matter(source_pages_path, output_pages_path, applied_dict,
                            args, verbose=args.verbose, markup=args.format)
        if args.verbose:
            print('front matter format:', args.format)
    else:
        # replace function call and use --test option to isolate output
        test_dict = OrderedDict({ 
        'title': 'test title', 'date': '2020-10-10', 'tags': ['1', '2', '3']})
        test_list = test_dict['tags']
        # print(mm.format_header(test_dict, 'default'))
        # print(mm.format_list_value(test_list, 'yaml'))
        print(mm.insert_auto_vals('../resources/elestel-converted/places/arcadia.md',
                            test_dict, markup='default'))

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
