#!/usr/bin/env python3

"""marksman arguments"""

import argparse
from textwrap import dedent

__author__ = "Ike Davis"
__copyright__ = "Copyright 2020, Ike Davis"
__license__ = "MIT"
__version__ = "v0.1 beta"


def build_args():
    parser = argparse.ArgumentParser(
        prog='marksman', description=dedent("""\
            %(prog)s formats the front matter of content files (usually 
            markdown files) to prepare them for use with static site generators 
            such as Hugo and Pelican. It formats front matter in YAML, TOML, and
            Pelican's Markdown metadata syntax, which is the default."""),
        epilog="""Author: Ike Davis\nLicense: MIT, NO warranties offered or implied""")
    parser.add_argument('--version', help='print version info then exit',
                        version='%(prog)s v0.1 beta', action='version')
    parser.add_argument('paths', nargs=2, metavar=('SOURCE, OUTPUT'),
                        help='path to the SOURCE files and path where OUTPUT files will go')
    parser.add_argument('--author', '-a', metavar=('AUTHOR'),
                        help='add AUTHOR for posts/pages')
    parser.add_argument('--authors', '-m', nargs='*', metavar=('AUTHORS'),
                        help='add AUTHORS for posts/pages')
    parser.add_argument('--category', '-c', metavar=('CATEGORY'),
                        help='add CATEGORY for posts/pages')
    parser.add_argument('--date', '-u', metavar=('DATE_TYPE'), choices=['created', 'filename', 'mod'],
                        default='created',
                        help='date taken from one of: created, mod, filename')
    parser.add_argument('--draft', '-d', const='true',
                        help='marks files as a Hugo draft', action='store_const')
    parser.add_argument('--format', '-f', metavar=('FORMAT'), choices=['default', 'yaml', 'toml', 'rst'],
                        default='default',
                        help='format for front matter. FORMATs: yaml, toml, rst, default')
    parser.add_argument('--json', '-j', metavar=('JSON'), 
                        help='path to a JSON file with front matter data.')
    parser.add_argument('--tags', '-t', nargs='*', metavar=('TAGS'),
                        help='add TAGS for posts/pages')
    parser.add_argument('--verbose', '-v',
                        help='get info on processed files', action='store_true')
    parser.add_argument('--test',
                        help='test method outputs', action='store_true')

    args = parser.parse_args()

    return args
